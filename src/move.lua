-- move provides some simple movement functions.

tArgs = { ... }
dir = tArgs[1]
dist = tArgs[2]
dig = tArgs[3]

dist = tonumber(dist)
dig = tonumber(dig)

if (dist == 0) then
  dist = 1
end

function up()
  while (dig == 1 and turtle.detectUp()) do
    turtle.digUp()
  end

  turtle.up()
end

function down()
  while (dig == 1 and turtle.detectDown()) do
    turtle.digDown()
  end

  turtle.down()
end

function forward()
  while (dig == 1 and turtle.detect()) do
    turtle.dig()
  end

  turtle.forward()
end

function turnAround()
  turtle.turnLeft()
  turtle.turnLeft()
end

for i = 1, dist do
  if (dir == "up" or dir == "Up" or dir == "UP") then
    up()
  elseif (dir == "down" or dir == "Down" or dir == "DOWN") then
    down()
  elseif (dir == "right" or dir == "Right" or dir == "RIGHT") then
    if (i == 1) then
      turtle.turnRight()
    end

    forward()

    if (i == dist) then
      turtle.turnLeft()
    end
  elseif (dir == "left" or dir == "Left" or dir == "LEFT") then
    if (i == 1) then
      turtle.turnLeft()
    end

    forward()

    if (i == dist) then
      turtle.turnRight()
    end
  elseif (dir == "fwd" or dir == "Fwd" or dir == "FWD" or dir == "forward" or dir == "Forward" or dir == "FORWARD") then
    forward()
  elseif (dig == 1 and (dir == "back" or dir == "Back" or dir == "BACK" or dir == "backward" or dir == "Backward" or dir == "BACKWARD")) then
    if (i == 1) then
      turnAround()
    end

    forward()

    if (i == dist) then
      turnAround()
    end
  elseif (dir == "back" or dir == "Back" or dir == "BACK" or dir == "backward" or dir == "Backward" or dir == "BACKWARD") then
    turtle.back()
  elseif (dir == "turnRight" or dir == "tRight" or dir == "tr" or dir == "tR" or dir == "TR") then
    turtle.turnRight()
  elseif (dir == "turnLeft" or dir == "tLeft" or dir == "tl" or dir == "tL" or dir == "TL") then
    turtle.turnLeft()
  end
end

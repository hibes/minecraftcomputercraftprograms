-- build part of monster spawner
-- bms

f_cnt = 0
b_cnt = 0
u_cnt = 0
d_cnt = 0

WATERL = 1
WATERR = 2

water_flag = 0

function fwd()
  turtle.forward()
  f_cnt = f_cnt + 1
end

function back()
  turtle.back()
  b_cnt = b_cnt + 1
end

function up()
  turtle.up()
  u_cnt = u_cnt + 1
end

function pd()
  turtle.placeDown()

  if (water_flag == 1) then
    turtle.turnLeft()
    turtle.place()
    turtle.turnRight()
  end

  if (water_flag == 2) then
    turtle.turnRight()
    turtle.place()
    turtle.turnLeft()
  end
end

function down()
  turtle.down()
  d_cnt = d_cnt + 1
end

function npg(dist)
  turtle.select(1)
 
  pd()
 
  for i = 1, dist do
    fwd()
    pd()
  end
end

tArgs = { ... }
 
flag = tArgs[1]

if (flag == "waterl") then
  flag = "water"
  water_flag = WATERL
end

if (flag == "waterr") then
  flag = "water"
  water_flag = WATERR
end

shell.run("refuel", "64")

if (flag == "" or flag == "std") then
  -- Main part
  --up()
  npg(6)

  fwd()
  pd()
  back()

  up()
  npg(7)
  up()
  npg(7)

  fwd()
  pd()
  back()

  
  up()
  npg(7)
  -- Last little bit
  up()
  pd()
  up()
  pd()
  for i = 1, 2 do
    fwd()
    down()
    down()
    pd()
    up()
    pd()
    up()
    pd()
  end
elseif (flag == "wall") then
  -- Main part
  up()
  npg(5)
  up()
  npg(7)
  up()
  npg(7)
  up()
  npg(6)
  -- Last little bit
  up()
  pd()
  up()
  pd()
  for i = 1, 2 do
    fwd()
    down()
    down()
    pd()
    up()
    pd()
    up()
    pd()
  end
elseif (flag == "water") then
  fwd()
  npg(7)
  up()
  npg(6)
  up()
  npg(7)
  up()
  npg(7)
  up()
  npg(1)
else
  error("invalid parameter")
end

shell.run("move", "back", f_cnt - b_cnt)
shell.run("move", "down", u_cnt - d_cnt)

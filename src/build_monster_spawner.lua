-- Henry Brown
-- This turtle program will make a massive mob spawner tower.  You can handle killing the mobs 
--  however you see fit.  
-- If you give it an odd sized shape (say ... 55x53), it will expand it until it makes a viable
-- square design (60x60) (it must be 4 more than a multiple of 14)
-- Forcing it to be square just makes coding this simpler.
-- Make sure these chunks stay loaded...
-- The turtle will always assume it's pointed in the positive Y direction

-- Get newest version
tArgs = { ... }

START_FLOOR = 1

if (#tArgs == 0) then
  shell.run("rm", "bmspawn")
  shell.run("pastebin", "get", "iL0YnST3", "bmspawn")
  shell.run("rm", "buildMonsterSpawner")
  shell.run("pastebin", "get", "iL0YnST3", "buildMonsterSpawner")
  return 0
elseif (#tArgs == 1 and tonumber(tArgs[1]) ~= nil) then
  START_FLOOR = tArgs[1]
elseif (#tArgs == 1) then
  shell.run("rm", "bmspawn")
  shell.run("pastebin", "get", "iL0YnST3", "bmspawn")
  shell.run("rm", "buildMonsterSpawner")
  shell.run("pastebin", "get", "iL0YnST3", "buildMonsterSpawner")
  shell.run("buildMonsterSpawner", tArgs[1])
end

-- X is where the turtle should go.
-- [OOOOOOOOOOOOOOOOOO]
-- [OOOOOOOOOOOOOOOOOO]
-- ...
-- [OOOOOOOOOOOOOOOOOO]
-- [XOOOOOOOOOOOOOOOOO]

-- That's position (1,1)


-- Assumptions:
--  Slots 1-15 contain stone (or whatever material you want your monster spawner made out of)
--  Slot 16 contains fuel if applicable, nothing otherwise

shell.run("refuel")

-- GLOBALS
orientationX = 1
orientationY = 1
orientationZ = 1

targetXSize = 0
targetYSize = 0

digbool = 0
tracking = 0
slot = 0
fl = {}

-- This is how often we repeat the structure (10 seems reasonable, will leave 
--  mobs at least 4-5 blocks above their head)
REP_FREQ = 10
-- This is how tall we want the structure to be.  
--  150 seems reasonable
-- Reduced to 80 because I don't want to write 
--  code to build a wall around the upper levels 
--  right now.
Z_HEIGHT = 80

-- =max(((floor((A1 - 4) / 14, 1) + 1) * 14 + 4), 18)
function validateGridSize(dist)
  if((dist - 4) % 14 == 0 and dist >= 18) then
    return dist
  else
    return ((math.floor((dist - 4) / 14) + 1) * 14 + 4);
  end
end

function fwd()
  if (tracking == 1) then
    orientationX = orientationX + 1
  end
  
  while (turtle.detect() or not turtle.forward()) do
    if (digBool == 0) then
      break
    end

    turtle.dig()
    sleep(.25)
  end
end

function bwd()
  if (tracking == 1) then
    orientationX = orientationX - 1
  end

  if (digBool > 0) then
    turtle.turnRight()
    turtle.turnRight()
  
    while (turtle.detect() or not turtle.forward()) do
      if (digBool == 0) then
        break
      end

      turtle.dig()
      sleep(.25)
    end

    turtle.turnLeft()
    turtle.turnLeft()
  else
    turtle.back()
  end
end

function right()
  if (tracking == 1) then
    orientationY = orientationY + 1
  end

  turtle.turnRight()
  
  while (turtle.detect() or not turtle.forward()) do
    if (digBool == 0) then
      break
    end

    turtle.dig()
    sleep(.25)
  end

  turtle.turnLeft()
end

function left()
  if (tracking == 1) then
    orientationY = orientationY - 1
  end

  turtle.turnLeft()
  
  while (turtle.detect() or not turtle.forward()) do
    if (digBool == 0) then
      break
    end

    turtle.dig()
    sleep(.25)
  end

  turtle.turnRight()
end

function up()
  orientationZ = orientationZ + 1

  while (turtle.detectUp() or not turtle.up()) do
    if (digBool == 0) then
      break
    end

    turtle.digUp()
    sleep(.25)
  end
end

function initializeFloorPlan(length)
  fl = {}

  for i=1,length do
    fl[i] = {}
    for j=1,length do
      fl[i][j] = {}
      for k=1,Z_HEIGHT do
        fl[i][j][k] = 0

        if (i > targetXSize or j > targetYSize) then
          fl[i][j][k] = "C"
        end
      end
    end
  end
end

function createWaterRun(x, y, z, xdir, ydir, halflw)
  if(xdir == ydir) then
    error("xdir == ydir")
  end

  local fstart
  local fend
  local fstep
  local xval
  local yval

  fstep = 1

  if(xdir > 0) then
    if(x < halflw) then
      error("Inconsistent parameters.")
    end
    fstart = x
    fend = halflw * 2
  elseif(xdir < 0) then
    if(x > halflw) then
      error("Inconsistent parameters.")
    end
    fstart = x
    fend = 1
    fstep = -1
  elseif(ydir > 0) then
    if(y < halflw) then
      error("Inconsistent parameters.")
    end
    fstart = y
    fend = halflw * 2
  elseif(ydir < 0) then
    if(y > halflw) then
      error("Inconsistent parameters.")
    end
    fstart = y
    fend = 1
    fstep = -1
  end

  for i=fstart,fend,fstep do
    if(xdir ~= 0) then
      xval = i
      yval = y
      if(xdir > 0) then
        zval = math.floor((i - fstart) / 7) + z
      else
        zval = math.floor((fstart - i) / 7) + z
      end
    else
      xval = x
      yval = i
      if(ydir > 0) then
        zval = math.floor((i - fstart) / 7) + z
      else
        zval = math.floor((fstart - i) / 7) + z
      end
    end

    if  ((fstart - i) % 7 == 0 and 
        zval > z) then
      zval = zval - 1
    end

    --write("x:")
    --write(xval)
    --write("  y:")
    --write(yval)
    --write("  z:")
    --write(zval)
    --write("\n")

    fl[xval][yval][zval] = "="
  end
end

function createWallRun(x, y, z, xdir, ydir, halflw)
  if(xdir == ydir) then
    error("xdir == ydir")
  end

  local fstart
  local fend
  local fstep
  local xval
  local yval

  fstep = 1

  if(xdir > 0) then
    if(x < halflw) then
      error("Inconsistent parameters.")
    end
    fstart = x
    fend = halflw * 2
  elseif(xdir < 0) then
    if(x > halflw) then
      error("Inconsistent parameters.")
    end
    fstart = x
    fend = 1
    fstep = -1
  elseif(ydir > 0) then
    if(y < halflw) then
      error("Inconsistent parameters.")
    end
    fstart = y
    fend = halflw * 2
  elseif(ydir < 0) then
    if(y > halflw) then
      error("Inconsistent parameters.")
    end
    fstart = y
    fend = 1
    fstep = -1
  end

  for i=fstart,fend,fstep do
    if(xdir ~= 0) then
      xval = i
      yval = y
      if(xdir > 0) then
        zval = math.floor((i - fstart) / 7) + z
      else
        zval = math.floor((fstart - i) / 7) + z
      end
    else
      xval = x
      yval = i
      if(ydir > 0) then
        zval = math.floor((i - fstart) / 7) + z
      else
        zval = math.floor((fstart - i) / 7) + z
      end
    end

    fl[xval][yval][zval] = "+"
    
    if ((fstart - i) % 7 == 0 and z < zval) then
      fl[xval][yval][zval - 1] = "+"
    end
  end
end

function print_z(length)
  local hex
  local tval
  local val

  write("print_z() started\n")

  for z=1,Z_HEIGHT do
    for y=1,length do
      if(z < 10) then
        write("z= ")
      else
        write("z=")
      end
      write(z)
      if(y < 10) then
        write("y= ")
      else
        write("y=")
      end
      write(y)
      write("{")

      if(length <= 32) then
        for x=1,length do
          if(fl[x][y][z] == 0) then
            write(" ")
          elseif(fl[x][y][z] == 1) then
            write("|")
          else
            write(fl[x][y][z])
          end
        end
      else
        for x=1,length,4 do
          tval = fl[x][y][z]
          if (tval ~= " " and
              tval ~= "C" and
              tval ~= 0) then
            val = 1
            hex = val * 8
          end

          if (x + 1 <= length) then
            tval = fl[x + 1][y][z]

            if (tval ~= " " and
                tval ~= "C" and
                tval ~= 0) then
              val = 1
              hex = hex + (val * 4)
            end

            if (x + 2 <= length) then
              tval = fl[x + 2][y][z]

              if (tval ~= " " and
                  tval ~= "C" and
                  tval ~= 0) then
                val = 1
                hex = hex + val * 2
              end

              if (x + 3 <= length) then
                tval = fl[x + 3][y][z]

                if (tval ~= " " and
                    tval ~= "C" and
                    tval ~= 0) then
                  val = 1
                  hex = hex + val
                end
              end
            end
          end

          if (hex < 10) then
            write(hex)
          elseif (hex == 10) then
            write("A")
          elseif (hex == 11) then
            write("B")
          elseif (hex == 12) then
            write("C")
          elseif (hex == 13) then
            write("D")
          elseif (hex == 14) then
            write("E")
          elseif (hex == 15) then
            write("F")
          end
        end
      end

      write("}\n")

      if(y % 16 == 0) then
        write("Continue (y/n):")
        resp = io.read()
        if(resp == "n") then
          break
        end
      end
    end
    write("Continue (y/n):")
    resp = io.read()
    if(resp == "n") then
      break
    end
  end
end

function fillFloorPlan(length)
  halflw = math.floor(length / 2)

  xL = halflw - 1
  xH = halflw + 2
  yL = halflw - 1
  yH = halflw + 2

  for z=1,Z_HEIGHT do
    if (z < 4) then
      for y=yL,yH do
        fl[xL][y][z] = 1
        fl[xH][y][z] = 1
      end
      for x=xL,xH do
        fl[x][yL][z] = 1
        fl[x][yH][z] = 1
      end
    else
      fl[xL][yL][z] = 1
      fl[xL][yH][z] = 1
      fl[xH][yL][z] = 1
      fl[xH][yH][z] = 1
    end
  end

  for z=4,Z_HEIGHT,REP_FREQ do
    createWaterRun(halflw,   yL, z, 0, -1, halflw)
    createWaterRun(halflw+1, yL, z, 0, -1, halflw)
    createWaterRun(halflw,   yH, z, 0,  1, halflw)
    createWaterRun(halflw+1, yH, z, 0,  1, halflw)

    for t=halflw,halflw+1 do
      createWaterRun(xL, t, z, -1, 0, halflw)
      createWaterRun(xH, t, z,  1, 0, halflw)
    end

    for t=halflw-2,halflw-1 do
      createWallRun(xL, t, z + 1, -1, 0, halflw)
      createWallRun(xL, t, z + 1, -1, 0, halflw)
      createWallRun(xH, t, z + 1,  1, 0, halflw)
      createWallRun(xH, t, z + 1,  1, 0, halflw)
    end

    for t=halflw+2,halflw+3 do
      createWallRun(xL, t, z + 1, -1, 0, halflw)
      createWallRun(xL, t, z + 1, -1, 0, halflw)
      createWallRun(xH, t, z + 1,  1, 0, halflw)
      createWallRun(xH, t, z + 1,  1, 0, halflw)
    end

    for j=1,halflw-1 do
      _z = z + 1 + math.floor(j / 7)
      yL = halflw - j
      yH = halflw + j + 1
  
      if(j % 4 == 1 or j % 4 == 2) then
        if((j) % 8 >= 5) then
          createWallRun(xL, yL, _z + 2, -1, 0, halflw)
          createWallRun(xL, yH, _z + 2, -1, 0, halflw)
          createWallRun(xH, yL, _z + 2,  1, 0, halflw) 
          createWallRun(xH, yH, _z + 2,  1, 0, halflw)
        end
  
        createWallRun(xL, yL, _z + 1, -1, 0, halflw)
        createWallRun(xL, yH, _z + 1, -1, 0, halflw)
        createWallRun(xH, yL, _z + 1,  1, 0, halflw) 
        createWallRun(xH, yH, _z + 1,  1, 0, halflw)
      else
        createWaterRun(xL, yL, _z, -1, 0, halflw)
        createWaterRun(xL, yH, _z, -1, 0, halflw)
        createWaterRun(xH, yL, _z,  1, 0, halflw)
        createWaterRun(xH, yH, _z,  1, 0, halflw)
      end
    end
  end
  
  --print_z(length)
end

function travelPerimeter()
  while not turtle.detect() do
    fwd()
    targetYSize = targetYSize + 1
  end

  turtle.turnRight()

  while not turtle.detect() do
    fwd()
    targetXSize = targetXSize + 1
  end

  shell.run("move", "back", targetXSize)

  turtle.turnLeft()

  shell.run("move", "back", targetYSize)
end

function buildFloorPlan(length, startZ, breakZ)
  clearFloor(length, startZ)

  for z=startZ,Z_HEIGHT do
    if (breakZ >= startZ and z > breakZ) then
      return 0
    end

    -- We want to place down, so we'll start
    --  above the relevant z coordinate.
    -- Placing down allows us to not worry about
    --  accidentally breaking our newly placed
    --  blocks.
    up()

    buildFloor(length, z)
  end
end

function buildFloor(length, z)
  for y=1,length do
    buildLine(length, y, z)
  end
end

function clearFloor(length, z)
  for y=1,length do
    clearLine(length, y, z)
  end
end

function clearLine(length, y, z)
  halflw = math.floor(length / 2)

  startX = 1
  endX   = length
  stepX  = 1

  moveTo(orientationX, orientationY, length, y)

  orientationX = orientationX +
                 length - orientationX
  orientationY = orientationY +
                 y - orientationY

  for x=startX,endX,stepX do
    if (fl[x][y][z] == "C") then
      write("Going from (")
      write(orientationX)
      write(",")
      write(orientationY)
      write(") to (")
      write(x)
      write(",")
      write(y)
      write(") to Clear\n")

      moveTo(orientationX, orientationY, x, y)

      orientationX = orientationX +
                     x - orientationX
      orientationY = orientationY +
                     y - orientationY

      break
    end
  end
end

function buildLine(length, y, z)
  halflw = math.floor(length / 2)

  if (orientationX <= halflw) then
    startX = 1
    endX   = length
    stepX  = 1
  else
    startX = length
    endX   = 1
    stepX  = -1
  end

  for x=startX,endX,stepX do
    if (fl[x][y][z] == "C") then
      write("Going from (")
      write(orientationX)
      write(",")
      write(orientationY)
      write(") to (")
      write(x)
      write(",")
      write(y)
      write(") to Clear\n")

      moveTo(orientationX, orientationY, x, y)

      orientationX = orientationX +
                     x - orientationX
      orientationY = orientationY +
                     y - orientationY
    elseif (fl[x][y][z] ~= 0 and fl[x][y][z] ~= " ") then
      write("Going from (")
      write(orientationX)
      write(",")
      write(orientationY)
      write(") to (")
      write(x)
      write(",")
      write(y)
      write(") to Place\n")

      moveTo(orientationX, orientationY, x, y)
      placeDown()

      orientationX = orientationX +
                     x - orientationX
      orientationY = orientationY +
                     y - orientationY
    end
  end
end

function placeDown()
  empty = false
  if (turtle.getItemCount(slot) < 1) then
    if (slot >= 15) then
      empty = true

      while empty do
        sleep(10)

        for i=1,slot-1 do
          if (turtle.getItemCount(i) >= 1) then
            turtle.select(i)
            empty = false
          end
        end
      end
    else
      selectSlot(slot + 1)
      placeDown()
    end
  else
    turtle.placeDown()
  end
end

function selectSlot(n)
  slot = n
  turtle.select(n)
end

function max(n1, n2)
  if (n1 > n2) then
    return n1
  else
    return n2
  end
end

function moveTo(fromX, fromY, toX, toY)
  if (fromX == toX) then
  elseif (fromX > toX) then
    if (digBool) then
      shell.run("move", "left", fromX-toX, 1)
    else
      shell.run("move", "left", fromX-toX)
    end
  else
    if (digBool) then
      shell.run("move", "right", toX-fromX, 1)
    else
      shell.run("move", "right", toX-fromX)
    end
  end
  
  if (fromY == toY) then
  elseif (fromY > toY) then
    if (digBool) then
      shell.run("move", "back", fromY-toY, 1)
    else
      shell.run("move", "back", fromY-toY)
    end
  else
    if (digBool) then
      shell.run("move", "fwd", toY-fromY, 1)
    else
      shell.run("move", "fwd", toY-fromY)
    end
  end
end

function countBlocks(LENGTH)
  sum = 0
  for x=1,LENGTH do
    for y=1,LENGTH do
      for z=1,Z_HEIGHT do
        if(fl[x][y][z] ~= 0 and 
           fl[x][y][z] ~= " " and
           fl[x][y][z] ~= "C") then
          sum = sum + 1
        end
      end
    end
  end

  write("Sum of blocks required: ")
  write(sum)
  write("\n")
end

function build()
  digBool = 0
  tracking = 0
  selectSlot(1)

  --travelPerimeter()

  targetXSize = 61
  targetYSize = 61

  LENGTH = validateGridSize(
             max(targetXSize + 1, targetYSize + 1)
           )

  write("x: ")
  write(targetXSize)
  write("\ny: ")
  write(targetYSize)
  write("\nlen: ")
  write(LENGTH)
  write("\n")

  initializeFloorPlan(LENGTH)
  fillFloorPlan(LENGTH)

  countBlocks(LENGTH)

  digBool = 1
  tracking = 1

  buildFloorPlan(LENGTH, 3, 0)

  write("done!")
end

build()

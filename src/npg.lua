tArgs = { ... }
 
dist = tArgs[1]

turtle.select(1)
turtle.up()

turtle.placeDown()

for i = 1, dist do
  turtle.forward()
  turtle.placeDown()
end

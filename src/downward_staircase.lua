-- downward_staircase does something involving building a staircase.  Presumably downward.

tArgs = { ... }
expArgs = { "Distance", "Width", "Height" }
types = { "i", "i", "i" }
trashTable = {}

TORCH_FREQ = 10
REFUEL_QUANT = 3
REFUEL_THRESHOLD = 20

function help()
  prog = shell.getRunningProgram()
  print("Help for " .. prog .. ":")
  print("Please note, $var[?] describes an expected argument")
  print("Additionally, [i] describes an expected integer, [s] an expected string, etc.")
  print("Your turtle should start in the lower left hand corner of your tunnel")

  for i = 1, #expArgs do
    prog = prog .. " " .. expArgs[i] .. "[" .. types[i] .. "]"
  end

  error(prog)
end

function sanity()
  if (#types ~= #expArgs) then
    error("Programmer error.  #types and #expArgs should be equal")
  end

  if (#tArgs == 0) or (tArgs[1] == "help") or (tArgs[1] == "-h") or (tArgs[1] == "--help") then
    help()
  end

  if (#tArgs ~= #expArgs) then
    if (#tArgs > #expArgs) then
      print("You had too many arguments")
    else
      print("You had too few arguments")
    end

    for i = 1, #tArgs do
      if (i <= #expArgs) then
        print(expArgs[i] .. ": " .. tArgs[i])
      else
        print("??: " .. tArgs[i])
      end
    end

    error("Incorrect number of arguments")
  end

  for i = 1, #types do
    if (types[i] == "i") then
      -- Integer value
      if (type(tonumber(tArgs[i])) ~= "number") then
        error("Cannot convert to number: " .. expArgs[i] .. "[" .. i .. "]")
      end
    end
  end
end

function min(val1, val2)
  if(val1 < val2) then
    return val1
  else
    return val2
  end

  return 0
end

function refuel()
  if (fuelRequired) then
    if (turtle.getFuelLevel() < REFUEL_THRESHOLD) then
      turtle.select(16)

      print("Adding Fuel..." .. turtle.getFuelLevel())

      while (turtle.getFuelLevel() < REFUEL_THRESHOLD and turtle.refuel(min(turtle.getItemCount(16), REFUEL_QUANT))) do
        sleep(.25)
      end

      print("Added Fuel..." .. turtle.getFuelLevel())

      turtle.select(1)

      if (turtle.getFuelLevel() < REFUEL_THRESHOLD) then
        error ("out of fuel -- please put fuel in slot 16 and try again.")
      end
    end
  end
end

sanity()

fuelRequired = (not (turtle.getFuelLevel() == "unlimited"))

if (fuelRequired) then
  refuel()

  lastTrashLocation = 15
else
  lastTrashLocation = 16
end

wsw = 0 -- Width switch
hsw = 0 -- Height switch
rfs = 0 -- Distance right traveled from start
ufs = 0 -- Distance up traveled from start
ffs = 0 -- Distance fwd traveled from start

rtrn = 0 -- Switch to 1 when it's time to return home

function up()
  refuel()

  while (turtle.detectUp() or not (turtle.up())) do
    turtle.digUp()
    sleep(.25)
  end
end

function down()
  refuel()

  while (turtle.detectDown() or not (turtle.down())) do
    turtle.digDown()
    sleep(.25)
  end
end

function fwd()
  refuel()

  while (turtle.detect() or not (turtle.forward())) do
    turtle.dig()
    sleep(.25)
  end
end

function hank_return()
  if (rfs > 0) then
    -- Assumes initial vector is facing opposite from return vector.
    turtle.turnLeft()

    for i = 1, rfs do
      fwd()
    end

    turtle.turnRight()
  end

  if (ufs > 0) then
    for j = 1, ufs do
      down()
    end
  end

  -- turn around to head back
  turtle.turnLeft()
  turtle.turnLeft()

  -- avoid those newly placed torches!
  up()

  for i = 1, ffs do
    fwd()
  end

  -- return to original location and vector.
  down()

  turtle.turnLeft()
  turtle.turnLeft()
end

-- TODO: Handle case where height = 1, right now it would just dig straight forward, and there may
--  be other unintended consequences.
function hank_move()
  for i = 1, tArgs[1] + 1 do
    -- For each distance
    for j = 1, tArgs[3] do
      -- For each height
      if (rtrn ~= 0) then
        hank_return()

        error("Returned home early...")
      end

      if (tonumber(tArgs[2]) > 1) then
        if (wsw == 0) then
          turtle.turnRight()
        else
          turtle.turnLeft()
        end

        for k = 1, tArgs[2] - 1 do
          -- For each width
          fwd()

          if (wsw == 0) then
            rfs = rfs + 1
          else
            rfs = rfs - 1
          end
        end

        if (wsw == 0) then
          turtle.turnLeft()
          wsw = 1
        else
          turtle.turnRight()
          wsw = 0
        end
      end

      if (rtrn ~= 0) then
        hank_return()
        error("Returned home early...")
      end

      if (j < tonumber(tArgs[3])) then
        if (hsw == 0) then
          up()

          ufs = ufs + 1
        else
          down()

          ufs = ufs - 1
        end
      end
    end

    if (hsw == 0) then
      hsw = 1
    else
      hsw = 0
    end

    if (i ~= tArgs[1] + 1) then
      fwd()

      ffs = ffs + 1

      if ((ufs == 0) and (ffs % TORCH_FREQ == 0 or ffs % TORCH_FREQ == 1)) then
        -- drop a torch
        turtle.turnLeft()
        turtle.turnLeft()
        turtle.select(1)
        turtle.place()
        turtle.turnLeft()
        turtle.turnLeft()
      end
    end

    ups = ufs - 1
    down()
  end
end

hank_move()
hank_return()

print("RFS: " .. rfs)
print("UFS: " .. ufs)
print("FFS: " .. ffs)

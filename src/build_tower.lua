-- Presumably builds some kind of tower.

function refuel()
  if (fuelRequired) then
    if (turtle.getFuelLevel() < REFUEL_THRESHOLD) then
      turtle.select(16)

      print("Adding Fuel..." .. turtle.getFuelLevel())

      while (turtle.getFuelLevel() < REFUEL_THRESHOLD and turtle.refuel(min(turtle.getItemCount(16), REFUEL_QUANT))) do
        sleep(.25)
      end

      print("Added Fuel..." .. turtle.getFuelLevel())

      turtle.select(1)

      if (turtle.getFuelLevel() < REFUEL_THRESHOLD) then
        error ("out of fuel -- please put fuel in slot 16 and try again.")
      end
    end
  end
end

function min(val1, val2)
  if(val1 < val2) then
    return val1
  else
    return val2
  end
 
  return 0
end

function up()
  refuel()
 
  while (digStatus and (turtle.detectUp() or not (turtle.up()))) do
    turtle.digUp()
    sleep(.25)
  end
end
 
function down()
  refuel()
 
  while (digStatus and (turtle.detectDown() or not (turtle.down()))) do
    turtle.digDown()
    sleep(.25)
  end
end
 
function fwd()
  refuel()
 
  while (digStatus and (turtle.detect() or not (turtle.forward()))) do
    turtle.dig()
    sleep(.25)
  end
end

function go()
  digStatus = 0
  fwd()
  digStatus = 1

  for i = 1, dist do
    up()

    turtle.placeDown()
    
    ufs = ufs + 1
  end
end

function rtrn()
  digStatus = 0

  if (ufs > 0) then
    turtle.turnRight()
    turtle.turnRight()

    fwd()

    turtle.turnRight()
    turtle.turnRight()

    for j = 1, ufs do
      down()
    end
  end

  digStatus = 1
end

tArgs = { ... }

dist = tArgs[1]

REFUEL_QUANT = 3
REFUEL_THRESHOLD = 20
digStatus = 1
fuelRequired = 1

ufs = 0

go()
rtrn()

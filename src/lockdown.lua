-- lockdown.lua is intended to manage access 

tArgs = { ... }

if (#tArgs == 0) then
  shell.run("rm", "lockdown")
  shell.run("pastebin", "get", "ZkDgneEY", "lockdown")
  shell.run("lockdown", "anyargumentwilldo")
  return 0
end

os.loadAPI("ocs/apis/sensor")

local RADIUS = 10
local offset = {
  X = 275,
  Y = 76,
  Z = 45
}

function distance(pos)
  local xd = pos.X - offset.X
  local yd = pos.Y - offset.Y
  local zd = pos.Z - offset.Z

  return math.sqrt(xd*xd+yd*yd+zd*zd)
end

local prox = sensor.wrap("bottom")
local targets
local closed = true
local count = 0
local resp = ""

while true do
  closed = true
  
  targets = prox.getTargets()

  for k, v in pairs(targets) do
    --for k, v in pairs(v) do
    --  write(textutils.serialize(k))
    --  write(":")
    --  write(textutils.serialize(v))
    --  write("\n")

    --  count = count + 1

    --  if(count % 10 == 0) then
    --    write("Continue? ([Y]/n):")
    --    resp = io.read()

    --    if (resp == "n") then
    --      error("Quitting program.")
    --    end
    --  end
    --end
    if (textutils.serialize(k) == "\"Shatonapples\"") then
      dist = distance(v.Position)

      if(dist < RADIUS) then
        closed = false
      end
    end
  end
  
  redstone.setOutput("top",    closed)
  redstone.setOutput("back",   closed)
end

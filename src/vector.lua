-- vector.lua

-- The intended functionality of this script is to take in a vector string (i.e. Mu15Ml20Md3Mr17) and move
--  accordingly.  Additional functionality will be added such as control characters M (Move), D (Dig), 
--  and P (Place), directional characters f (forward), u (up), d (down), b (back), r (right), l (left)
-- 
-- These techniques could be used to build structures, or even dig tunnels / quarries in a relatively user-friendly
--  manner.

-- CONTROL CHARACTERS
C_MOVE  = "M"
C_DIG   = "D"
C_PLACE = "P"

-- SERIALIZATION VALUES
-- Control characters can be combined for multiple functionality
S_NOOP   = 0

S_MOVE   = 1
S_MOVED  = 2
S_MOVEU  = 4

S_DIG    = 8
S_DIGD   = 16
S_DIGU   = 32

S_PLACE  = 64
S_PLACED = 128
S_PLACEU = 256

-- Need to find a more elegant solution to getting values out of the arg list.
arg_vectorValue = tArgs[1]

vector = {}

function vector.serializeVectorString(self, vectorValue)
    
end

vectorTable = vector.serializeVectorString(vector, arg_vectorValue)
